
# 1- Install and start the Mocked Database APIs

```
npm install -g json-server

json-server --watch ./mock-api/db.json --port 3004
```
And:
```
npm install -g graphql-faker

graphql-faker --open ./mock-api/schema.facker.graphql
```
## 2- Install and Run the project

In the project directory:

`yarn install`

`yarn start`

*It will open a navigator page, **YOU CAN PUT WHATEVER IN USER AND PASSWORD LOGIN**

**In Chrome, with F12 you can test the responsiveness in many different mobile and tablet screens.**

## 3- Test the unit tests

`yarn test`


### Build for producction

 `yarn build`

Builds the app for production to the `build` folder.<br />

**You can see a test production example at** http://hectorferris.com/prueba/cecotec-test/
