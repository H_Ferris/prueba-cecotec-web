const colors = {
  darkBackground:  "#282c34",
  mainBackground: "#F7FCFB",
  boxBackground: "#F7F7F7",
  transparent: "rgba(0,0,0,0)",

  success: "#66D145",
  error: "#F21B46",
  warning: "#FDA419",

  main: "#3eb1c8",
  main50: "#3eb1c880",
  main30: "#3eb1c84D",

  black: "#212625",
  black80: "#212625CC",
  black50: "#21262580",
  black30: "#2126254D",


  gray: "#7f7f7f",
  gray50: "#7f7f7f80",
  gray30: "#7f7f7f4D",

  lightGray: "#AAB1AE",
  highlight: "#68d4ba",

  aqua: "#257181",
  aqua50: "#25718180",
  aqua30: "#2571814D",

  white: "#ffffff",
  white50: "#ffffff80",
  white30: "#ffffff4D",
};

export default colors;
