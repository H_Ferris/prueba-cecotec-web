import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';

import Config from "./config";

const uri = Config.gqlBaseUri;

const client = new ApolloClient({
    link: new HttpLink({ uri }),
    cache: new InMemoryCache()
});

export default client;