const env = "local"; // "dev"; // "local"; // "production"; // <--Cambiar aquí!

console.log("Enviroment >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", env);

let apiBase, gqlBaseUri;

switch (env) {
    case "dev":
        apiBase = "https://gorest.co.in/public-api/";
        gqlBaseUri = "https://api.github.com/graphql/";
        break;

    case "local":
        apiBase = "http://127.0.0.1:3004/";
        gqlBaseUri = "http://localhost:9002/graphql/";
        break;

    case "production":
        apiBase = "https://whateverurlinproduction.com/";
        gqlBaseUri = "https://whateverurlinproduction.com/";
        break;

    default:
        break;
}

export default {
    env,
    apiBase,
    gqlBaseUri,
};