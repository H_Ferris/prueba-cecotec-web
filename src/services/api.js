import { create } from 'apisauce'

import Config from "./config";

const api = create({
    baseURL: Config.apiBase,
    headers: {
        "Content-Type": "application/json"
    },
    timeout: 30000
});

api.setAuthToken = token => {
    if (token === null) {
        delete api.headers["Authorization"];
    } else {
        api.setHeader("Authorization", token);
    }
};

api.getUsers = async () => {
    return api.get(`users`);
}

api.deleteUser = async (id) => {
    return api.delete(`users/${id}`);
}

api.createUser = async (newUser) => {
    return api.put(`users`, newUser);
}

api.updateUser = async (user) => {
    return api.patch(`users/${user.id}`, user);
}

api.doLogin = async (email, password) => {
    if (Config.env === "local") return "local";
    return api.post("authentication/", {
        email,
        password
    });
};

export default api;
