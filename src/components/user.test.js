import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import User from './user';

const user = {
  "id": 11,
  "name": "Chandraayan Dwivedi",
  "email": "dwivedi_chandraayan@ratke-sipes.io",
  "gender": "Male",
  "status": "Active",
  "created_at": "2020-10-07T03:50:04.281+05:30",
  "updated_at": "2020-10-07T03:50:04.281+05:30"
};

const deleteUser = jest.fn();
const updateUser = jest.fn();

test('renders user', () => {
  const component = render(<User user={user} delUser={deleteUser} saveUser={updateUser} />);
  expect(component.getByText('Chandraayan Dwivedi')).toBeInTheDocument();
  fireEvent.click(component.getByText('Delete'));
  expect(deleteUser).toHaveBeenCalledWith(11);
  fireEvent.click(component.getByText('Save'));
  expect(updateUser).toHaveBeenCalledWith(user);
});