import React from 'react';
import styled from 'styled-components';
import { Colors, Metrics } from '../theme/index'

const View = styled.div`
min-width:${Metrics.images.image}px;
align-self: center;
  text-align: center;
  margin: 10px;
  @media (max-width:767px) {
    min-width: ${Metrics.images.imageMini}px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`
const Title = styled.p`
  font-size: ${Metrics.fonts.body1}px;
  color: ${Colors.white};
  @media (max-width:767px) {
    font-size: ${Metrics.fonts.body2}px;
  }
`
const Email = styled.p`
  font-size: ${Metrics.fonts.small}px;
  color: ${Colors.lightGray};
  @media (max-width:767px) {
    font-size: ${Metrics.fonts.mini}px;
  }
`

const Image = styled.img`
  height        : ${Metrics.images.image}px;
  border-radius: ${Metrics.images.image / 2}px;
  @media (max-width:767px) {
    height: ${Metrics.images.imageMini}px;
    border-radius: ${Metrics.images.imageMini / 2}px;
  }
`

function UserRow({ user }) {
  return (
    <View>
      <Title>{user.name} {user.status === 'Active' ? <span role="img" aria-label="jsx-a11y/aria-proptypes">&#x2714;</span> : <span role="img" aria-label="jsx-a11y/aria-proptypes">&#x274C;</span>}</Title>
      <Image
        src={'https://loremflickr.com/300/300/person?nocache=' + user.id.toString()} />
      <Email>{user.email}</Email>
    </View>
  )
}

export default UserRow;
