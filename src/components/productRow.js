import React from 'react';
import styled from 'styled-components';
import { Colors, Metrics } from '../theme/index'

const View = styled.div`
  min-width:${Metrics.images.image}px;
  align-self: center;
  text-align: center;
  margin: 10px;
  @media (max-width:767px) {
    min-width: ${Metrics.images.imageMini}px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`
const Title = styled.p`
  font-size: ${Metrics.fonts.body1}px;
  color: ${Colors.white};
  @media (max-width:767px) {
    font-size: ${Metrics.fonts.body2}px;
  }
`

const Image = styled.img`
  height: ${Metrics.images.image}px;
  border-radius: ${Metrics.borderRadius}px;
  @media (max-width:767px) {
    height: ${Metrics.images.imageMini}px;
  }
`

function ProductRow({ product }) {
  return (
    <View>
      <Title>{product.name}</Title>
      <View>
        <Image
          src={product.image + "?id=" + product.id.toString()} />
      </View>
    </View>
  )
}

export default ProductRow;
