import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import TopNav from './topnav';

const navigateTo =  jest.fn();

test('Change page in top nav function', () => {
  const { getByText } = render(<TopNav navigateTo={navigateTo} />);
  fireEvent.click(getByText('Products'));
  expect(navigateTo).toHaveBeenCalledWith('products');
  fireEvent.click(getByText('Users'));
  expect(navigateTo).toHaveBeenCalledWith('users');
});
