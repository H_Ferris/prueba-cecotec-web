import React from 'react';
import styled from 'styled-components';
import { Colors, Metrics } from '../theme/index'

const View = styled.div`
  align-self: center;
  text-align: center;
  margin: ${Metrics.smallSpace}px;
`
const Title = styled.p`
  font-size: ${Metrics.fonts.h1}px;
  color: ${Colors.white};
  font-weight: bold;
`
const Image = styled.img`
  height: ${Metrics.images.imageBig}px;
  border-radius: ${Metrics.borderRadiusBig}px;
`
const TextBox = styled.textarea`
  align-self: center;
  text-align: center;
  padding: 1vmin;
  width: 50vmax;
  height: 30vmax;
  font-size: ${Metrics.fonts.body1}px;
  color: ${Colors.lightGray};
  margin: ${Metrics.miniSpace}px;
`
const Delete = styled.button`
  align-self: center;
  margin: 10px;
  display: inline;
  color: ${Colors.white};
  background-color: ${Colors.error};
`
const Save = styled.button`
  align-self: center;
  margin: 10px;
  display: inline;
  color: ${Colors.white};
  background-color: ${Colors.success};
`
class Product extends React.Component {
  constructor() {
    super();
    this.state = { text: "" };
  }

  componentDidMount = () => {
    const { product } = this.props;
    this.setState({ text: product.description });
  }

  componentDidUpdate(prevProps) {
    const { product } = this.props;

    if (product.id !== prevProps.product.id) {
      this.setState({ text: product.description });
    }
  }

  saveProduct = () => {
    const { product, saveProduct } = this.props;
    const { text } = this.state;

    let newProduct = Object.assign({}, product);
    newProduct.description = text;
    saveProduct(newProduct);
  }

  render() {
    const { product, delProduct } = this.props;
    const { text } = this.state;

    return (
      <View>
        <View>
          <Title>{product.name}</Title>
          <View>
            <Image
              src={product.image + "?id=" + product.id.toString()} />
          </View>
          <Title>{product.price}$</Title>
          <TextBox value={text} onChange={text => this.setState({ text: text.target.value })} />
        </View>
        <Delete type="button" onClick={() => delProduct(product.id)}>Delete</Delete>
        <Save type="button" onClick={() => this.saveProduct()}>Save</Save>
      </View>
    )
  }
}

export default Product;
