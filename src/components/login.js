import React from 'react';
import styled from 'styled-components';

import API from "../services/api";
import logo from '../images/logo.png';
import Loading from './loading';
import { Colors, Metrics } from '../theme/index'

const View = styled.div`
  align-self: center;
  text-align: center;
  height: 100%;
  width: 100%;
`
const Logo = styled.img`
  margin-top: 10vmin;
  height: 40vmin;
  border-radius: 20vmin;
`
const TextInput = styled.input`
  width: 200px;
  text-align: center;
  font-size:${Metrics.fonts.body1}px;
  padding: ${Metrics.plusSpace}px;
  border-radius: ${Metrics.baseSpace}px;
  border: 1px solid ${Colors.main};
`
const Button = styled.button`
  align-self: center;
  text-align: center;
  padding: 10px 30px;
  border-radius: 10px;
  font-size:${Metrics.fonts.body1}px;
  border: 1px solid ${Colors.main};
  margin: 10px;
`

class Login extends React.Component {
  constructor() {
    super();
    this.state = { user: "", pass: "", session: false, loading: false };
  }

  doLogin = async (user, pass) => {
    const { navigateTo } = this.props;

    this.setState({ loading: true });
    API.doLogin(user, pass).then(session => {
      if (session !== "local") {
        console.log(session)
        API.setAuthToken(session.token);
        this.setState({ loading: true });
        navigateTo("users");
      } else {
        this.setState({ loading: true });
        navigateTo("users")
      };
    });
  }

  render() {
    const { user, pass, loading } = this.state;

    return (
      <View>
        <Logo src={logo} alt="logo" />
        <h1>Your user.</h1>
        <TextInput type="text" onChange={text => this.setState({ user: text.target.value })} />
        <h1>Your password.</h1>
        <TextInput type="password" onChange={text => this.setState({ pass: text.target.value })} onKeyDown={e => { if (e.key === 'Enter') this.doLogin(user, pass) }} />
        <br />
        <br />
        {loading ? <Loading />
          :
          <Button type="button" onClick={() => this.doLogin(user, pass)}>Login</Button>
        }
      </View>
    )
  }
}

export default Login;
