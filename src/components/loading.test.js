import React from 'react';
import { render } from '@testing-library/react';
import Loading from './loading';

test('render loading component', () => {
  const { getByTestId } = render(<Loading />);
  expect(getByTestId("loading")).toBeInTheDocument();
});
