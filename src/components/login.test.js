import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import Login from './login';

const navigateTo = jest.fn();

test('Do Login function', async () => {
  const component = render(<Login navigateTo={navigateTo} />);
  await waitForElement(() => component.getByText('Login'));
  fireEvent.click(component.getByText('Login'));
  await waitForElement(() => component.getByTestId('loading'));
  expect(navigateTo).toHaveBeenCalledWith('users');
});
