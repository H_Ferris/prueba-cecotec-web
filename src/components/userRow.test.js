import React from 'react';
import { render } from '@testing-library/react';
import UserRow from './userRow';

const user = {
  "id": 11,
  "name": "Chandraayan Dwivedi",
  "email": "dwivedi_chandraayan@ratke-sipes.io",
  "gender": "Male",
  "status": "Active",
  "created_at": "2020-10-07T03:50:04.281+05:30",
  "updated_at": "2020-10-07T03:50:04.281+05:30"
};


test('renders userRow', () => {
  const { getByText } = render(<UserRow user={user} />);
  const component = getByText('Chandraayan Dwivedi');
  expect(component).toBeInTheDocument();
});

