import React from 'react';
import styled from 'styled-components';

import { Colors, Metrics } from '../theme/index'

const View = styled.div`
  align-self: center;
  text-align: center;
  padding: 10px;
`
const Title = styled.p`
  font-size: ${Metrics.fonts.h1}px;
  color: ${Colors.white};
  font-weight: bold;
`
const Image = styled.img`
  height: ${Metrics.images.imageBig}px;
  border-radius: ${Metrics.images.imageBig / 2}px;
`
const Email = styled.p`
  font-size: ${Metrics.fonts.body1}px;
  color: ${Colors.lightGray};
`
const Gender = styled.p`
  font-size: ${Metrics.fonts.h2}px;
  color: ${Colors.white};
  font-weight: bold;
`
const Status = styled.p`
  font-size: ${Metrics.fonts.h1}px;
  color: ${Colors.error};
  font-weight: bold;
  ${({ active }) => active && `
  color: ${Colors.success};
`}
`
const Delete = styled.button`
  align-self: center;
  margin: 10px;
  display: inline;
  color: ${Colors.white};
  background-color: ${Colors.error};
`
const Save = styled.button`
  align-self: center;
  margin: 10px;
  display: inline;
  color: ${Colors.white};
  background-color: ${Colors.success};
`

class User extends React.Component {
  constructor() {
    super();
    this.state = {
      checked: true,
      status: null
    };
  }

  componentDidMount() {
    const { user } = this.props;

    this.setState({ checked: user.status === 'Active' ? true : false, status: user.status });
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;

    if (user.id !== prevProps.user.id) {
      this.setState({ checked: user.status === 'Active' ? true : false, status: user.status });
    }
  }

  saveUser = () => {
    const { user, saveUser } = this.props;
    const { status } = this.state;

    let newUser = Object.assign({}, user);
    newUser.status = status;
    saveUser(newUser);
  }

  toggleStatus = (value) => {
    this.setState({ checked: value, status: value ? "Active" : "Inactive" });
  }


  render() {
    const { user, delUser } = this.props;
    const { checked, status } = this.state;

    return (
      <View>
        <View>
          <Title>{user.name}</Title>
          <View>
            <Image
              src={'https://loremflickr.com/300/300/person?nocache=' + user.id} />
          </View>
          <Gender>Gender: {user.gender}</Gender>
          <Email>{user.email}</Email>

          <Status active={checked}><input type="checkbox" name="checked" value={checked} checked={checked} onChange={event => this.toggleStatus(event.target.checked)} /> Status: {status}</Status>
        </View>
        <Delete type="button" onClick={() => delUser(user.id)}>Delete</Delete>
        <Save type="button" onClick={() => this.saveUser()}>Save</Save>
      </View>
    )
  }
}

export default User;
