import React from 'react';
import styled from 'styled-components';
import loguito from '../images/loguito.jpg';

const View = styled.div`
  width: 100%;
  display: flex;
  flex-direction : row;
`
const Logo = styled.img`
  max-width: 40vmin;
  margin: 5vmin;
  align-self: flex-start;
`
const Button = styled.button`
  align-self: center;
  margin: auto;
  display: inline;
`

function TopNav({ navigateTo }) {

  return (
    <View>
      <Logo src={loguito} className="App-logito" alt="logo" />
      <Button type="button" onClick={() => navigateTo("users")}>Users</Button>
      <Button type="button" onClick={() => navigateTo("products")}>Products</Button>
    </View>
  )
}

export default TopNav;
