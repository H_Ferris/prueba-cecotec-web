import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Product from './product';

const product = {
  "id": 1,
  "name": "Enormous Granite Chair",
  "description": "Denuncio curia barba trucido concedo tremo degenero umerus vilis congregatio aptus stabilis iusto tabernus aurum custodia adsuesco bonus adficio doloribus earum beatae cubicularis strues unde ascit crapula crux vitium omnis laboriosam utor callide stella iure verbum peior vergo expedita et vis verumtamen apto absum pariatur demo sunt acer.",
  "image": "https://lorempixel.com/250/250",
  "price": "23630.68",
  "discount_amount": "3038.81",
  "status": true,
  "categories": [
    {
      "id": 6,
      "name": "Computers & Toys"
    },
    {
      "id": 4,
      "name": "Books, Kids & Industrial"
    },
    {
      "id": 8,
      "name": "Music, Garden & Clothing"
    }
  ]
};

const deleteProduct = jest.fn();
const updateProduct = jest.fn();


test('renders product', () => {
  const component = render(<Product product={product} delProduct={deleteProduct} saveProduct={updateProduct} />);
  expect(component.getByText('Enormous Granite Chair')).toBeInTheDocument();
  fireEvent.click(component.getByText('Delete'));
  expect(deleteProduct).toHaveBeenCalledWith(1);
  fireEvent.click(component.getByText('Save'));
  expect(updateProduct).toHaveBeenCalledWith(product);
});
