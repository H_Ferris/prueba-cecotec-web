import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders login button', () => {
  const { getByText } = render(<App />);
  const login = getByText('Login');
  expect(login).toBeInTheDocument();
});
