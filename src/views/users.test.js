import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import Users from './users';


test('Load users and set one user', async () => {
  const component = render(<Users />);
  await waitForElement(() => component.getByTestId('loading'));
  await waitForElement(() => component.getAllByTestId("set-user"));
  fireEvent.click(component.getAllByTestId("set-user")[0]);
  expect(component.getByText("Edit")).toBeInTheDocument();
});