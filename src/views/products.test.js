import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import Products from './products';

test('Load products and set one product', async () => {
  const component = render(<Products />);
  await waitForElement(() => component.getByTestId('loading'));
  await waitForElement(() => component.getAllByTestId("set-product"));
  fireEvent.click(component.getAllByTestId("set-product")[0]);
  expect(component.getByText("Edit")).toBeInTheDocument();
});
