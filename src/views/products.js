import React from 'react';

import client from "../services/graphql";
import { gql } from '@apollo/client';
import Product from '../components/product'
import ProductRow from '../components/productRow'
import Loading from '../components/loading';
import logo from '../images/logo.png';
import './products.scss';

class Products extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: false,
            products: [],
            product: {}
        };
    }

    componentDidMount = async () => {
        this.setState({ loading: true });

        client.query({
            query: gql`
          {
            allProducts{
              id,
              name,
              image,
              price,
              description
            }
          }
          `
        }).then(result => {
            console.log(result);
            this.setState({ products: result.data.allProducts, loading: false });
        });

    }

    setProduct(id) {
        const { products } = this.state;

        console.log("Setting Product: ", id);
        let found = products.find(x => x.id === id);
        this.setState({ product: found })
    }

    deleteProduct = async (id) => {
        const { products } = this.state;

        this.setState({ loading: true });

        client.mutate({
            variables: { id },
            mutation: gql`
      mutation deleteProduct($id: ID!){
        deleteProduct(
            id: $id
          ) {
            id
          }
      }
      `}).then(result => {
                console.log("Product deleted -> ", result.data);
                let newProducts = products.filter(x => x.id !== id);
                this.setState({ products: newProducts, product: {}, loading: false });
            }).catch(error => { console.log(error) });

    }

    updateProduct = async (product) => {
        const { products } = this.state;

        this.setState({ loading: true });

        client.mutate({
            variables: { id: product.id, description: product.description },
            mutation: gql`
      mutation updateProduct($id: ID!, $description: String!){
        updateProduct(
            id: $id
            description: $description
          ) {
            id,
            description
          }
      }
      `}).then(result => {
                console.log("Product updated -> ", result.data);
                let newProducts = Object.assign([], products);
                newProducts[newProducts.indexOf(newProducts.find(x => x.id === product.id))] = Object.assign({}, product);
                this.setState({ products: newProducts, product, loading: false });
            }).catch(error => { console.log(error) });

    }

    render() {
        const { loading, products, product } = this.state;

        return (
            <>
                <div className="Products-sidebar">
                    {loading && <Loading />}
                    {
                        products.map(element =>
                            <div key={element.id}
                                data-testid="set-product"
                                className={(element.id === product.id) ? "Product-area Active" : "Product-area"}
                                onClick={() => this.setProduct(element.id)}
                            >
                                {(element.id === product.id) && <span className="active-prod">Edit</span>}
                                <ProductRow product={element} />
                            </div>
                        )
                    }
                </div>
                <div className="Product-container">
                    {Object.keys(product).length ? <Product product={product} delProduct={this.deleteProduct} saveProduct={this.updateProduct} /> : <img src={logo} className="App-logo" alt="logo" />}
                </div>
            </>
        )
    }

}

export default Products;
