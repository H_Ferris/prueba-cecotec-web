import React from 'react';

import API from "../services/api";
import User from '../components/user'
import UserRow from '../components/userRow'
import Loading from '../components/loading';
import logo from '../images/logo.png';
import './users.scss';

class Users extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: false,
            users: [],
            user: {}
        };
    }

    componentDidMount = async () => {
        this.setState({ loading: true });

        API.getUsers().then(response => {
            console.log("Users response -> ", response);
            if (response.status === 200 && response.ok && response.data) {
                this.setState({ users: response.data, loading: false });
            } else {
                if (response.problem === 'NETWORK_ERROR') {
                    alert('No Internet: ' + response.problem);
                } else {
                    alert('Error: ' + response.originalError ? response.originalError.toString() : response.problem);
                }
            }
        });
    }


    setUser(id) {
        const { users } = this.state;

        console.log("Setting User: ", id);
        let found = users.find(x => x.id === id);
        this.setState({ user: found })
    }

    deleteUser = async (id) => {
        const { users } = this.state;

        this.setState({ loading: true });
        API.deleteUser(id).then(response => {
            if (response.status === 200 && response.ok && response.data) {
                console.log("User deleted -> ", response);
                let newUsers = users.filter(x => x.id !== id);
                this.setState({ users: newUsers, loading: false, user: {} });
            } else {
                if (response.problem === 'NETWORK_ERROR') {
                    alert('No Internet: ' + response.problem);
                } else {
                    alert('Error: ' + response.originalError ? response.originalError.toString() : response.problem);
                }
            }
        });
    }

    updateUser = async (user) => {
        const { users } = this.state;

        this.setState({ loading: true });
        API.updateUser(user).then(response => {
            if (response.status === 200 && response.ok && response.data) {
                console.log("User updated -> ", response);
                let newUsers = Object.assign([], users);
                newUsers[newUsers.indexOf(newUsers.find(x => x.id === user.id))] = Object.assign({}, user);
                this.setState({ users: newUsers, loading: false, user });
            } else {
                if (response.problem === 'NETWORK_ERROR') {
                    alert('No Internet: ' + response.problem);
                } else {
                    alert('Error: ' + response.originalError ? response.originalError.toString() : response.problem);
                }
            }
        });
    }

    render() {
        const { loading, users, user } = this.state;
        return (
            <>
                <div className="Users-sidebar">
                    {loading && <Loading />}
                    {
                        users.map(element =>
                            <div key={element.id}
                                data-testid="set-user"
                                className={(element.id === user.id) ? "User-area Active" : "User-area"}
                                onClick={() => this.setUser(element.id)}
                            >
                                {(element.id === user.id) && <span className="active-usr">Edit</span>}
                                <UserRow user={element} />
                            </div>
                        )
                    }
                </div>
                <div className="User-container">
                    {Object.keys(user).length ? <User user={user} delUser={this.deleteUser} saveUser={this.updateUser} /> : <img src={logo} className="App-logo" alt="logo" />}
                </div>
            </>
        )
    }
}

export default Users;
