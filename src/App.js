import React from 'react';

import './App.scss';
import Login from "./components/login";
import Users from "./views/users";
import Products from "./views/products";
import TopNav from './components/topnav';

class App extends React.Component {
  constructor() {
    super();
    this.state = { page: null };
  }

  changePage = page => {
    this.setState({ page });
  }

  render() {
    const { page } = this.state;

    if (page === "products") return (
      <div className="App" >
        <header className="App-header">
          <TopNav navigateTo={this.changePage} />
        </header>
        <div className="App-container">
          <Products />
        </div>
      </div>
    );
    if (page === "users") return (
      <div className="App" >
        <header className="App-header">
          <TopNav navigateTo={this.changePage} />
        </header>
        <div className="App-container">
          <Users />
        </div>
      </div>
    );
    if (!page) return (
      <div className="App" >
        <Login navigateTo={this.changePage} />
      </div>
    );
  }
}

export default App;
